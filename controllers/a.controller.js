const redis_client = require('../config/redis');
const catchError = require('../middlewares/catchError');
const services = require('../services').block;
const errors = require('../helpers/apiError');
const sha1OfJson = require('../utils/sha1OfJson');

exports.getAll = catchError(async (req, res, next) => {
  const result = await services.getAll();
  res.json({ result });
});
