const redis_client = require('../config/redis');
const sha1OfJson = require('../utils/sha1OfJson');
const errors = require('../helpers/apiError');

module.exports = isParam => (req, res, next) => {
  const redisKey = isParam ? sha1OfJson(req.params) : sha1OfJson(req.query);

  redis_client.get(redisKey, (err, data) => {
    if (err) {
      next(errors.internal_error);
    }

    if (data != null) {
      res.json(JSON.parse(data));
    } else {
      next();
    }
  });
};
