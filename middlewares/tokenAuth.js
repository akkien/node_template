const jwt = require('jsonwebtoken');
const fs = require('fs');
const Joi = require('@hapi/joi');
const appRoot = require('app-root-path');
const errors = require('../helpers/apiError');
const publicKey = 'kien'; //fs.readFileSync(`${appRoot}/rsaKeys/public.pem`, 'utf8');

const joiPayload = Joi.object({
  clientId: Joi.string().required(),
  scope: Joi.string()
    .valid('internal', 'external')
    .required(),
  iat: Joi.number().required(),
  exp: Joi.number().required(),
  aud: Joi.array()
    .has(Joi.string().valid('https://read-api.vietnamblockchain.asia'))
    .required(),
  iss: Joi.string()
    .valid('VBC')
    .required(),
  sub: Joi.string().required(),
  jti: Joi.string().required()
});

module.exports = async (req, res, next) => {
  const authHeader = req.header('Authorization');

  //Check for token
  if (!authHeader) {
    return next(errors.required_auth);
  }

  const authHeaderArray = authHeader.split(' ');

  if (authHeaderArray[0] !== 'Bearer' || authHeaderArray.length == 0) {
    return next(
      errors.required_auth.withDetails('Authorization header is invalid')
    );
  }

  const token = authHeaderArray[1];

  jwt.verify(token, publicKey, async (err, payload) => {
    if (err) {
      if (err.message === 'jwt expired') {
        return next(errors.token_expired);
      }
      return next(errors.invalid_auth.withDetails(err.message));
    }

    try {
      await joiPayload.validateAsync(payload);
    } catch (error) {
      return next(errors.invalid_auth.withDetails(error.message));
    }

    req.tokenPayload = payload;
    next();
  });
};
