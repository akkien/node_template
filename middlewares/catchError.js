const errors = require('../helpers/apiError');

module.exports = function(handler) {
  return async (req, res, next) => {
    try {
      await handler(req, res, next);
    } catch (error) {
      next({
        userError: errors.internal_error.withDetails(error.message),
        realError: error
      });
    }
  };
};
