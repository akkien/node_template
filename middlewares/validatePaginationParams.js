const validator = require('validator');
const Joi = require('@hapi/joi');

const errors = require('../helpers/apiError');

const JoiPaginationParam = Joi.object({
  limit: Joi.number()
    .integer()
    .min(1)
    .max(100)
    .required(),
  page: Joi.number()
    .integer()
    .min(1)
    .required()
});

module.exports = function(req, res, next) {
  let { limit, page } = req.query;

  let JoiRes = JoiPaginationParam.validate({ limit, page });
  if (JoiRes.error) {
    return next(errors.invalid_input.withDetails(JoiRes.error.message));
  }

  limit = validator.toInt(limit);
  page = validator.toInt(page);

  req.query.limit = limit;
  req.query.page = page;
  req.query.offset = limit * page - limit;

  next();
};
