var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
var compression = require('compression');

const mountRoutes = require('./routes');
const winston = require('./config/winston');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(
  logger('combined', {
    stream: {
      write: function(message) {
        winston.info(message);
      }
    }
  })
);

app.use(helmet());
app.use(cors());
app.use(compression());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

mountRoutes(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // log error
  let errToLog = err.realError ? err.realError : err;
  let errToReturn = err.realError ? err.userError : err;

  winston.error(
    `${errToLog.stack}
    Header: ${JSON.stringify(req.headers)}
    Params: ${JSON.stringify(req.params)}
    Query: ${JSON.stringify(req.query)}
    Body: ${JSON.stringify(req.body)}
    Return to user: ${JSON.stringify(errToReturn)}`
  );

  return res.status(200).json({
    error: errToReturn
  });
});

var port = process.env.PORT || 9090;
app.listen(port, () => {
  winston.info(`Server listening on port ${port}`);
});
app.on('error', error => {
  winston.error('Error: ', error);
});

module.exports = app;
