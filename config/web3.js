const Web3 = require('web3');
require('dotenv').config();

const web3 = new Web3(
  new Web3.providers.WebsocketProvider(process.env.BLOCKCHAIN_WEBSOCKET_URI)
);

module.exports = web3;
