const redis = require('redis');
const port = process.env.REDIS_PORT || 6379;

module.exports = redis.createClient(port);
