const { Pool } = require('pg');
require('dotenv').config();

var connectionString = process.env.PG_CONNECT_STRING;

const pool = new Pool({ connectionString });

module.exports = {
  query: (text, params) => pool.query(text, params)
};
