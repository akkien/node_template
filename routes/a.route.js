const express = require('express');
const router = express.Router();

const controllers = require('../controllers');
const validatePaginationParams = require('../middlewares/validatePaginationParams');
const cache = require('../middlewares/cache');
const tokenAuth = require('../middlewares/tokenAuth');

// block
router.get('/hello', controllers.block.getAll);

module.exports = router;
