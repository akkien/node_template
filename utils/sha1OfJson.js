const crypto = require('crypto');

module.exports = json => {
  const sha1 = crypto.createHash('sha1');
  sha1.update(JSON.stringify(json));
  return sha1.digest('hex');
};
